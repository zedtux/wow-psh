# frozen_string_literal: true

#
# Vehicle represents the ships you can buy on WorldOfWarships Premium Shop.
#
class Vehicle < Ruson::Base
  field :name
  field :url
  field :currency
  field :price, class: Ruson::Float
  field :base_sale_price, name: 'baseSalePrice', class: Ruson::Float
  field :sale_price, name: 'salePrice', class: Ruson::Float
  field :discount_amount, name: 'discountAmount', class: Ruson::Float
  field :discount_percents, name: 'discountPercents'
  field :expired_at, name: 'expiredAt', class: Ruson::Time
  field :nations, class: Ruson::Array
  field :types, class: Ruson::Array
  field :levels, class: Ruson::Array
  field :bundle, class: Ruson::Boolean
  field :final_price, class: Ruson::Float
  field :final_price_history, class: Ruson::Array
end
