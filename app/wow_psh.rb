# frozen_string_literal: true

Dir.glob(File.join('./app/models', '*.rb')).each { |file| require file }

require './app/interactors/organise_ships_data_update'

module App
  #
  # Wow PSH loader
  #
  class WowPsh
    def self.run
      interactor = OrganiseShipsDataUpdate.call(build_organizer_arguments)

      if interactor.success?
        puts 'WOW Psh database update SUCCESS!'
      else
        puts 'ERROR: The WOW Psh database update failed: ' \
             "#{interactor.error.inspect}"

        exit 1
      end
    end

    def self.build_organizer_arguments
      {
        ci: ENV['CI'],
        app_root: ENV['APP_ROOT'],
        bot_email: ENV['BOT_EMAIL'],
        bot_name: ENV['BOT_NAME'],
        remote_url: ENV['GIT_REMOTE_URL'],
        ssh_private_key: ENV['SSH_PRIVATE_KEY']
      }
    end
  end
end
