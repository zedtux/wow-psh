# frozen_string_literal: true

module Git
  #
  # Configures the git client, setting the local project git config for the
  # username and email.
  #
  class ConfigureGitEmailAndUser
    include Interactor

    def call
      return unless context.ci == 'true'

      sanity_checks!

      puts "[#{self.class.name}] Configuring Git username and email ..."

      git_config('name', context.bot_name)
      git_config('email', context.bot_email)
    end

    private

    def sanity_checks!
      context.fail!(error: { bot_email: 'is missing' }) unless context.bot_email

      return if context.bot_name

      context.fail!(error: { bot_name: 'is missing' })
    end

    def git_config(key, value)
      git = Git::Command.call(git_ssh: context.git_ssh,
                              args: ['config', "user.#{key} '#{value}'"])

      context.fail!(error: git.error) if git.failure?

      puts git.output
    end
  end
end
