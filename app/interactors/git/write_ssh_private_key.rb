# frozen_string_literal: true

module Git
  #
  # Writes the given SSH key to the given SSH folder.
  #
  class WriteSshPrivateKey
    include Interactor

    def call
      return unless context.ci == 'true'

      sanity_checks!

      puts "[#{self.class.name}] Writing #{private_key_path} file ..."
      File.open(private_key_path, 'w') { |f| f.write(context.ssh_private_key) }
      FileUtils.chmod(0400, private_key_path, verbose: true)
    end

    private

    def sanity_checks!
      return if context.ssh_private_key

      context.fail!(error: { ssh_private_key: 'is missing' })
    end

    def private_key_path
      File.join(context.ssh_folder_path, 'id_rsa')
    end
  end
end
