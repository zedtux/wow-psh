# frozen_string_literal: true

module Git
  #
  # Checkout and pull the `master` branch of the git repository
  #
  class CheckoutMasterBranch
    include Interactor

    def call
      return unless context.ci == 'true'

      checkout_master && pull
    end

    private

    def checkout_master
      puts "[#{self.class.name}] Checking out the master branch ..."

      git = Git::Command.call(git_ssh: context.git_ssh,
                              args: %w[checkout master])
      context.fail!(error: git.error) if git.failure?
      puts git.output
    end

    def pull
      git = Git::Command.call(git_ssh: context.git_ssh, args: %w[pull])
      context.fail!(error: git.error) if git.failure?
      puts git.output
    end
  end
end
