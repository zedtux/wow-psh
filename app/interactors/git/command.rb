# frozen_string_literal: true

require 'open3'

module Git
  #
  # Run git commands using `GIT_SSH` env variable to pass a different SSH config
  # and check for the command output.
  #
  class Command
    include Interactor

    def call
      sanity_checks!

      build_git_command && run_git_command

      return if git_command_ran_successfully?

      fail_interactor_with_git_exit_code!
    end

    private

    def sanity_checks!
      context.fail!(error: { git_ssh: 'is missing' }) unless context.git_ssh

      return if context.args

      context.fail!(error: { args: 'is missing' })
    end

    def build_git_command
      context.command = ["GIT_SSH=#{context.git_ssh}", 'git', *context.args]
    end

    def command_has_sensitive_data?
      context.command.detect { |arg| arg =~ /email/ }
    end

    def fail_interactor_with_git_exit_code!
      context.fail!(
        error: {
          git_command: "ERROR: \`#{context.command.join(' ')}\` failed: " \
                       "#{context.process.exitstatus}"
        }
      )
    end

    def git_command_ran_successfully?
      context.process.exitstatus.zero?
    end

    def run_git_command
      if command_has_sensitive_data?
        puts "[#{self.class.name}] Executing command hidden git command ..."
      else
        puts "[#{self.class.name}] Executing command " \
             "#{context.command.join(' ')} ..."
      end

      context.output, context.process = Open3.capture2 context.command.join(' ')
    end
  end
end
