# frozen_string_literal: true

module Git
  #
  # Check if there are any updates on the `db/` folder's files, and commit/push
  # them if any.
  #
  class CommitAndPushDbFilesIfAnyUpdates
    include Interactor

    def call
      return unless context.ci == 'true'

      log_db_git_updates

      return unless db_folder_has_new_or_updated_files?

      commit_and_push_db_folder
    end

    private

    def commit_and_push_db_folder
      puts "[#{self.class.name}] Committing files from db/ ..."

      git_add_db_folder

      git_commit_files

      git_push
    end

    def db_folder_has_new_or_updated_files?
      context.git_status_output =~ %r{\t([\w\:]+)?\s*db\/.*$}
    end

    def git_add_db_folder
      git = Git::Command.call(git_ssh: context.git_ssh,
                              args: ['add', 'db/'])
      context.fail!(error: git.error) if git.failure?
      puts git.output
    end

    def git_commit_files
      git = Git::Command.call(git_ssh: context.git_ssh,
                              args: ['commit', '-m "Updates database files"'])
      context.fail!(error: git.error) if git.failure?
      puts git.output
    end

    def git_push
      git = Git::Command.call(git_ssh: context.git_ssh, args: ['push'])
      context.fail!(error: git.error) if git.failure?
      puts git.output
    end

    def log_db_git_updates
      git = Git::Command.call(git_ssh: context.git_ssh, args: ['status'])
      context.fail!(error: git.error) if git.failure?
      context.git_status_output = git.output

      puts context.git_status_output
    end
  end
end
