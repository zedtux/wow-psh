# frozen_string_literal: true

module Git
  #
  # Fails the interactor in the case the SSH key is missing, and remove newlines
  # from the key.
  #
  class EnsureSshKeysEnvironnmentVariablesAreDefined
    include Interactor

    def call
      return unless context.ci == 'true'

      sanity_checks!

      context.ssh_private_key = context.ssh_private_key
                                       .gsub('\'', '')
                                       .gsub('\n', "\n") + "\n"
    end

    private

    def sanity_checks!
      return if context.ssh_private_key

      context.fail!(error: { ssh_private_key: 'is missing' })
    end
  end
end
