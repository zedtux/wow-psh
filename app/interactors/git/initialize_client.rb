# frozen_string_literal: true

module Git
  #
  # Build the ssh_wrapper script path, and save it to the intractor context.
  # This script is used by Git::Command in order to change the SSH configuration
  # like the SSH Key path.
  #
  class InitializeClient
    include Interactor

    def call
      return unless context.ci == 'true'

      sanity_checks!

      context.git_ssh = File.join(context.app_root, 'bin', 'ssh_wrapper')
    end

    private

    def sanity_checks!
      return if context.app_root

      context.fail!(error: { app_root: 'is missing' })
    end
  end
end
