# frozen_string_literal: true

module Git
  #
  # Updates the git repository remote URLs in order to use the Git+ssh one,
  # instead of the Git+https.
  #
  class UpdateGitRemotes
    include Interactor

    def call
      return unless context.ci == 'true'

      sanity_checks!

      update_git_remote_urls
    end

    private

    def sanity_checks!
      return if context.remote_url

      context.fail!(error: { remote_url: 'is missing' })
    end

    def update_git_remote_urls
      puts "[#{self.class.name}] Updating Git repository remotes ..."

      git = Git::Command.call(
        git_ssh: context.git_ssh,
        args: ['remote', 'set-url', 'origin', context.remote_url]
      )
      context.fail!(error: git.error) if git.failure?
      puts git.output
    end
  end
end
