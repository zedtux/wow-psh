# frozen_string_literal: true

module Git
  #
  # Creates the `.ssh/` folder if required
  #
  class CreateSshFolderIfMissing
    include Interactor

    def call
      return unless context.ci == 'true'

      sanity_checks!

      context.ssh_folder_path = File.join(context.app_root, '.ssh')

      return if File.directory?(context.ssh_folder_path)

      FileUtils.mkdir(context.ssh_folder_path)
    end

    private

    def sanity_checks!
      return if context.app_root

      context.fail!(error: { app_root: 'is missing' })
    end
  end
end
