# frozen_string_literal: true

require 'json'

module WorldOfWarships
  #
  # Converts the context's data attribute from a String to a JSON Hash
  #
  class ParseJsonDataFromPremiumShop
    include Interactor

    def call
      context.data = JSON.parse(context.data)
    rescue JSON::ParserError => error
      context.fail!(
        error: {
          data_parsing_failed: error.message
        }
      )
    end
  end
end
