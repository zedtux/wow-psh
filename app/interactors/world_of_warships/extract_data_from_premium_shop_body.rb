# frozen_string_literal: true

module WorldOfWarships
  #
  # Extracts the JavaScript data variable from the HTML and store it in the
  # context.
  #
  class ExtractDataFromPremiumShopBody
    include Interactor

    DATA_REGEX = /window\.Data\.items\s*=\s*(.*)/.freeze

    def call
      context.data = context.body.scan(DATA_REGEX).flatten.first

      if context.data
        context.data = context.data.delete_suffix(';')
        data_extract_success
        return
      end

      data_extract_failure!
    end

    private

    def data_extract_failure!
      context.fail!(
        error: {
          data_extract_failed: 'No data found from the Premium shop HTTP body'
        }
      )
    end

    def data_extract_success
      context.body = nil # Free memory

      puts "[#{self.class.name}] WorldOfWarships Premium Shop data " \
           'extracting SUCCESS!'
    end
  end
end
