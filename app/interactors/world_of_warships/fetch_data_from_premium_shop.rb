# frozen_string_literal: true

module WorldOfWarships
  #
  # Download the ships catalog from the WOW Premium shop and store it in the
  # context.
  #
  class FetchDataFromPremiumShop
    include Interactor

    PREMIUM_SHOP_URL = 'https://eu.wargaming.net/shop/wows/vehicles/'

    def call
      context.body = fetch_data_from_premium_shop

      return unless context.body

      puts "[#{self.class.name}] WorldOfWarships Premium Shop body " \
           'fetching SUCCESS!'
    end

    private

    def fetch_data_from_premium_shop
      response = Faraday.get(PREMIUM_SHOP_URL)

      return response.body if response.status == 200

      context.fail!(
        error: {
          data_fetching_failed: "GET request to #{PREMIUM_SHOP_URL} ended " \
                                "with status #{response.status} " \
                                "(#{response.message}): #{response.value}"
        }
      )
    end
  end
end
