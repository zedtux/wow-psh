# frozen_string_literal: true

module WowPsh
  #
  # Updates the Vehicle final_price and final_price_history.
  #
  class UpdateVehiclesHistoricalPrice
    include Interactor

    def call
      context.data.each do |id, _|
        vehicle = Vehicle.find(id)

        real_price = evaluate_vehicle_price(vehicle)

        log_vehicle_price(vehicle, real_price)

        # When there is no price update, skip to the next vehicle
        next if vehicle.final_price == real_price

        update_vehicle_price_history(vehicle, real_price)
      end
    end

    private

    def evaluate_vehicle_price(vehicle)
      # sale_price is present when a vehicle promotion is ongoing
      vehicle.sale_price.positive? ? vehicle.sale_price : vehicle.price
    end

    def log_vehicle_price(vehicle, real_price)
      puts "[#{self.class.name}] Vehicle #{vehicle.name} (ID: #{vehicle.id}) " \
           "real_price: #{real_price.inspect} " \
           "(vehicle.sale_price: #{vehicle.sale_price.inspect}, " \
           "vehicle.price: #{vehicle.price.inspect})"
    end

    def update_vehicle_price_history(vehicle, real_price)
      if vehicle.final_price.positive?
        puts "[#{self.class.name}]  => Saving price #{vehicle.final_price} " \
             "at #{1.day.ago} ..."
        # Moves the previous price in the history and update the current price
        vehicle.final_price_history << {
          date: 1.day.ago,
          price: vehicle.final_price
        }
      end

      puts "[#{self.class.name}]  -> Updating vehicle final_price to " \
           "#{real_price} ..."
      vehicle.final_price = real_price
      vehicle.save
    end
  end
end
