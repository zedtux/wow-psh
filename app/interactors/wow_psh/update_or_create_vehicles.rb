# frozen_string_literal: true

module WowPsh
  #
  # Go through each vehicles from the Premium shop data, and create the new
  # vehicles.
  #
  class UpdateOrCreateVehicles
    include Interactor

    def call
      puts "[#{self.class.name}] Processing #{context.data.size} Vehicles ..."

      context.vehicle_mapping = {}

      context.data.each do |id, attributes|
        vehicle = create_or_update_vehicle(id, attributes)

        if attributes[:filterTags].nil?
          puts "[#{self.class.name}] WARNING: filterTags is nil for Vehicle " \
               "#{vehicle.name} (ID: #{vehicle.id})!"
        end

        unless id == vehicle.id.to_s
          puts "[#{self.class.name}] WARNING: The vehicle ID (#{vehicle.id}) " \
               "is different than the current ID (#{id}) which means " \
               'the vehicle has been updated on the WOW side.'

          # Fills in the `context.vehicle_mapping` with the IDs to be updated,
          # so that the `context.data` hash will be updated and next interactors
          # can rely on the `context.data` ID.
          #
          # WOW are publishing with a new ID updated vehicles so we can't rely
          # on the ID.
          # Instead wow-psh rely on the name (that's what you do when visiting
          # the WOW premium shop to check for vehicule updates), but searching
          # by name is time consuming as Ruson (the JSON ORM) needs to go
          # through all the files until it finds the vehicle record.
          #
          # At this moment in the code, Ruson has already wast resources in
          # searching the right vehicle, so we want to update the `context.data`
          # so that next interactors will use the ID, which is super cheap for
          # Ruson.
          #
          # Another interator updates the `context.data` keys (which are the
          # vehicles ID) based on the `context.vehicle_mapping` Hash.
          context.vehicle_mapping[id] = vehicle.id
        end

        next unless attributes[:filterTags]

        update_vehicle_tags(vehicle, attributes)
      end
    end

    private

    def create_or_update_vehicle(id, attributes)
      vehicle = Vehicle.where(name: attributes['name']).first

      if vehicle
        puts "[#{self.class.name}] Updating Vehicle with ID #{id} ..."
        vehicle.update(attributes)
      else
        puts "[#{self.class.name}] Creating Vehicle with " \
             "#{attributes.inspect} ..."
        vehicle = Vehicle.create({ id: id }.merge!(attributes))
      end

      vehicle
    end

    def update_vehicle_tags(vehicle, attributes)
      # When this is a bundle of ships
      vehicle.bundle = attributes[:filterTags]['type'].size > 1

      if attributes[:filterTags]['level'].empty?
        puts "WARNING: filterTags level is empty for Vehicle #{vehicle.name} " \
             "(ID: #{vehicle.id})!"

        return
      end

      vehicle.levels = attributes[:filterTags]['level']
      vehicle.nations = attributes[:filterTags]['nation']
      vehicle.types = attributes[:filterTags]['type']

      vehicle.save
    end
  end
end
