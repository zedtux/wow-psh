# frozen_string_literal: true

module WowPsh
  #
  # Replaces `context.data` keys where the vehicle ID must be updated.
  #
  # WOW are publishing with a new ID updated vehicles so we can't rely on
  # the ID.
  # Instead wow-psh rely on the name (that's what you do when visiting
  # the WOW premium shop to check for vehicule updates), but searching by name
  # is time consuming as Ruson (the JSON ORM) needs to go through all the files
  # until it finds the vehicle record.
  #
  # WowPsh::UpdateOrCreateVehicles interactor has created a Hash with the IDs
  # that must be changed, and this interactor replaces `context.data` keys based
  # on that Hash.
  #
  class UpdateVehicleIds
    include Interactor

    def call
      context.vehicle_mapping.each do |wow_id, vehicle_id|
        puts "[#{self.class.name}] ♻️  Replacing data ID #{wow_id} with " \
             "#{vehicle_id} ..."

        context.data[vehicle_id] = context.data.delete(wow_id)
      end
    end
  end
end
