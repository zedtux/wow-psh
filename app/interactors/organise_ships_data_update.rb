# frozen_string_literal: true

Dir.glob(File.join('./app/interactors', '**', '*.rb')).each do |file|
  require file
end

#
# Organises fetching, parsing and saving the Wow Premium shop data.
#
class OrganiseShipsDataUpdate
  include Interactor::Organizer

  organize Git::EnsureSshKeysEnvironnmentVariablesAreDefined,
           Git::CreateSshFolderIfMissing,
           Git::WriteSshPrivateKey,
           Git::InitializeClient,
           Git::ConfigureGitEmailAndUser,
           Git::UpdateGitRemotes,
           Git::CheckoutMasterBranch,
           WorldOfWarships::FetchDataFromPremiumShop,
           WorldOfWarships::ExtractDataFromPremiumShopBody,
           WorldOfWarships::ParseJsonDataFromPremiumShop,
           WowPsh::UpdateOrCreateVehicles,
           WowPsh::UpdateVehicleIds,
           WowPsh::UpdateVehiclesHistoricalPrice,
           Git::CommitAndPushDbFilesIfAnyUpdates
end
