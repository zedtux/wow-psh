# frozen_string_literal: true

require './config/application'

if ENV['RUBY_ENV'] == 'dev'
  puts 'Ruby ENV'
  puts '========'
  ENV.keys.sort.each { |key| puts "#{key} #{ENV[key]}" }
end

App::WowPsh.run
