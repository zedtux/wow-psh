# Word Of Warships (WOW) - Premium Shop History (PSH)

Wow-psh is a very small application, running using the Gitlab pipelines, which collects the [World Of Warships](https://worldofwarships.eu)' ships data from their [Premium shop](https://eu.wargaming.net/shop/wows/main/) and build a history JSON files database.

## TODO

 - Check : http://recharts.org/

# Development

In order to run the projet locally please follow those instructions:

1. Install Earthly: https://docs.earthly.dev/installation
2. Create a `.run-env` file like this:
```
CI=true
BOT_EMAIL=<your bot's email>
BOT_NAME=<your bot's name>
GIT_REMOTE_URL=git@gitlab.com:<username>/wow-psh.git
SSH_PRIVATE_KEY=<Git private key>
```
3. Run `earth -P +run`

## Loading models through IRB

```
$ docker-compose run app irb
irb(main):001:0> require 'ruson'
=> true
irb(main):002:0> require './app/models/vehicle'
=> true
irb(main):003:0> Ruson.output_folder = './db/'
=> "./db/"
irb(main):004:0> Vehicle.first
=> #<Vehicle:0x0000562c26508928 @name="West Virginia", @url="/shop/wows/vehicles/11599/", @currency="EUR", @price="24.01", @base_sale_price=nil, @sale_price=nil, @discount_amount=nil, @discount_percents=nil, @expired_at=nil, @nations=["usa"], @types=["Battleship"], @levels=["6"], @bundle=nil, @id=11599>
```
