FROM ruby:2.6.3-alpine3.9
WORKDIR /application

deps:
    RUN apk update \
        && apk upgrade \
        && apk add --update --no-cache git \
                                       tzdata
    SAVE IMAGE

gems:
    FROM +deps

    COPY Gemfile* /application

    RUN touch ~/.gemrc \
        && echo "gem: --no-ri --no-rdoc" >> ~/.gemrc \
        && gem install rubygems-update \
        && update_rubygems \
        && gem install bundler:$(cat Gemfile.lock \
                                 | grep BUNDLED -A 1 \
                                 | tail -n 1 \
                                 | awk '{print $1}') \
        && bundle install --gemfile=/application/Gemfile \
                          --jobs $(nproc)

    SAVE ARTIFACT /usr/local/bundle bundler

prod:
    FROM +deps

    ENV APP_ROOT /application
    ENV RAILS_ENV production

    COPY +gems/bundler /usr/local/bundle

    COPY .git /application/.git
    COPY app /application/app
    COPY bin /application/bin
    COPY config /application/config
    COPY db /application/db
    COPY Gemfile* /application
    COPY wow_psh.rb /application

    RUN addgroup -S wow \
        && adduser -s /bin/sh \
                   -G wow \
                   -S \
                   -u 1000 \
                   wow \
        && chown -R wow:wow /application

    USER wow

    ENTRYPOINT ["bundle", "exec"]
    CMD ["ruby", "wow_psh.rb"]

    SAVE IMAGE registry.gitlab.com/zedtux/wow-psh:latest

dev:
    FROM +prod

    ENV RAILS_ENV development

    RUN chown -R wow:users /usr/local/bundle

    SAVE IMAGE registry.gitlab.com/zedtux/wow-psh:dev

run:
    FROM docker:19.03.13-dind

    ARG CI="false"
    ARG BOT_EMAIL="youremail@here.me"
    ARG BOT_NAME="Your Bot Name"
    ARG GIT_REMOTE_URL="git@<GIT URL>/wow-psh.git"
    ARG SSH_PRIVATE_KEY="-----BEGIN OPENSSH PRIVATE KEY-----<Your key here>-----END OPENSSH PRIVATE KEY-----"

    RUN env | sort

    WITH DOCKER
        DOCKER PULL registry.gitlab.com/zedtux/wow-psh:latest

        RUN docker run --env CI="$CI" \
                       --env BOT_EMAIL="$BOT_EMAIL" \
                       --env BOT_NAME="$BOT_NAME" \
                       --env GIT_REMOTE_URL="$GIT_REMOTE_URL" \
                       --env SSH_PRIVATE_KEY="$SSH_PRIVATE_KEY" \
                       zedtux/wow-psh:latest
    END
