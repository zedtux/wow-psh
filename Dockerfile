#
# Builds a temporary image, with all the required dependencies used to compile
# the dependencies' dependencies.
# This image will be destroyed at the end of the build command.
#
FROM ruby:2.6.3-alpine3.9 AS build-env

ARG APP_ROOT=/application/
ARG BUILD_PACKAGES=""
ARG DEV_PACKAGES="git"
ARG RUBY_PACKAGES="tzdata"

RUN apk update && \
    apk upgrade && \
    apk add --update --no-cache $BUILD_PACKAGES \
                                $DEV_PACKAGES \
                                $RUBY_PACKAGES && \
    mkdir -p $APP_ROOT

WORKDIR $APP_ROOT

COPY Gemfile* $APP_ROOT

RUN git clone https://github.com/zedtux/ruson.git /ruson \
    && cd /ruson \
    && git checkout features/querying-by-attributes

RUN touch ~/.gemrc && \
    echo "gem: --no-ri --no-rdoc" >> ~/.gemrc && \
    gem install rubygems-update && \
    update_rubygems && \
    gem install bundler && \
    bundle install --jobs $(nproc) && \
    rm -rf /usr/local/bundle/cache/*.gem && \
    find /usr/local/bundle/gems/ -name "*.c" -delete && \
    find /usr/local/bundle/gems/ -name "*.o" -delete

COPY . $APP_ROOT

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Builds the final image with the minimum of system packages
# and copy the gem's sources, Bundler gems and Yarn packages.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FROM ruby:2.6.3-alpine3.9

LABEL maintainer="zedtux"

ARG APP_ROOT=/application/
ARG PACKAGES="bash git tzdata"

ENV APP_ROOT=$APP_ROOT

RUN apk update && \
    apk upgrade && \
    apk add --update --no-cache $PACKAGES && \
    mkdir -p $APP_ROOT

WORKDIR $APP_ROOT

# Add a group or add a user to a group
#
#  -g GID   Group id
#  -S       Create a system group
#
# Create new user, or add USER to GROUP
#
#  -h DIR   Home directory
#  -g GECOS GECOS field
#  -s SHELL Login shell
#  -G GRP   Group
#  -S       Create a system user
#  -D       Don't assign a password
#  -H       Don't create home directory
#  -u UID   User id
#  -k SKEL  Skeleton directory (/etc/skel)
RUN addgroup -S \
             wow && \
    adduser -s /bin/sh \
            -G wow\
            -S \
            -u 1000 \
            wow && \
    chown -R wow:wow $APP_ROOT

COPY --from=build-env /usr/local/bundle/ /usr/local/bundle/
COPY --chown=wow:wow --from=build-env $APP_ROOT $APP_ROOT

USER wow

ENTRYPOINT ["bundle", "exec"]
CMD ["ruby", "wow_psh.rb"]
